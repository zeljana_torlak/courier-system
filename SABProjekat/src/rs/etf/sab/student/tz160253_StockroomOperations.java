package rs.etf.sab.student;

import rs.etf.sab.student.entities.Adresa;
import rs.etf.sab.student.entities.LokacijaMagacina;
import rs.etf.sab.student.entities.Paket;
import rs.etf.sab.student.entities.Vozilo;
import java.util.List;
import javax.persistence.EntityManager;
import rs.etf.sab.operations.StockroomOperations;

public class tz160253_StockroomOperations implements StockroomOperations {
    EntityManager em = tz160253_GeneralOperations.em;

    @Override
    public int insertStockroom(int address) {
        Adresa addr = em.find(Adresa.class, address);
        if (addr == null)
            return -1;
        
        List<LokacijaMagacina> resultList = em.createQuery("SELECT lm FROM LokacijaMagacina lm WHERE lm.adresa.idA IN (SELECT a.idA FROM Adresa a WHERE a.grad.idG = :idG)", LokacijaMagacina.class).setParameter("idG", addr.getGrad().getIdG()).getResultList();
        if (resultList != null && !resultList.isEmpty())
            return -1;
        
        LokacijaMagacina stockroom = new LokacijaMagacina();
        stockroom.setAdresa(addr);
        em.getTransaction().begin();
        em.persist(stockroom);
        em.getTransaction().commit();
        return stockroom.getIdLM();
    }

    @Override
    public boolean deleteStockroom(int idStockroom) {
        LokacijaMagacina stockroom = em.find(LokacijaMagacina.class, idStockroom);
        if (stockroom == null)
            return false;
        
        List<Vozilo> vehiclesParkedList = em.createQuery("SELECT v FROM Vozilo v WHERE v.lokacijaParkirano.idA = :idA AND v.idV NOT IN (SELECT vz.vozilo.idV from Vozi vz)", Vozilo.class).setParameter("idA", stockroom.getAdresa().getIdA()).getResultList();
        if (vehiclesParkedList != null && !vehiclesParkedList.isEmpty())
            return false;
        
        List<Paket> packagesInStockroom = em.createQuery("SELECT p FROM Paket p WHERE p.trenutnaLokacija.idA = :idA AND p.idP NOT IN (SELECT pz.paket.idP from PrevoziSe pz WHERE pz.redniBroj<=0)", Paket.class).setParameter("idA", stockroom.getAdresa().getIdA()).getResultList();
        if (packagesInStockroom != null && !packagesInStockroom.isEmpty())
            return false;
        
        em.getTransaction().begin();
        em.remove(stockroom);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public int deleteStockroomFromCity(int idCity) {
        List<LokacijaMagacina> resultList = em.createQuery("SELECT lm FROM LokacijaMagacina lm WHERE lm.adresa.idA IN (SELECT a.idA FROM Adresa a WHERE a.grad.idG = :idG)", LokacijaMagacina.class).setParameter("idG", idCity).getResultList();
        if (resultList == null || resultList.isEmpty())
            return -1;

        LokacijaMagacina stockroom = resultList.get(0);        
        int idStockroom = stockroom.getIdLM();
        
        List<Vozilo> vehiclesParkedList = em.createQuery("SELECT v FROM Vozilo v WHERE v.lokacijaParkirano.idA = :idA AND v.idV NOT IN (SELECT vz.vozilo.idV from Vozi vz)", Vozilo.class).setParameter("idA", stockroom.getAdresa().getIdA()).getResultList();
        if (vehiclesParkedList != null && !vehiclesParkedList.isEmpty())
            return -1;
        
        List<Paket> packagesInStockroom = em.createQuery("SELECT p FROM Paket p WHERE p.trenutnaLokacija.idA = :idA AND p.idP NOT IN (SELECT pz.paket.idP from PrevoziSe pz WHERE pz.redniBroj<=0)", Paket.class).setParameter("idA", stockroom.getAdresa().getIdA()).getResultList();
        if (packagesInStockroom != null && !packagesInStockroom.isEmpty())
            return -1;
        
        em.getTransaction().begin();
        em.remove(stockroom);
        em.getTransaction().commit();
        return idStockroom;
    }

    @Override
    public List<Integer> getAllStockrooms() {
        return em.createQuery("SELECT lm.idLM FROM LokacijaMagacina lm", Integer.class).getResultList();
    }
    
}
