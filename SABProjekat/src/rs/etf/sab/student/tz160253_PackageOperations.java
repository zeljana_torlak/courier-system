package rs.etf.sab.student;

import rs.etf.sab.student.entities.Adresa;
import rs.etf.sab.student.entities.Korisnik;
import rs.etf.sab.student.entities.Paket;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import javax.persistence.EntityManager;
import rs.etf.sab.operations.PackageOperations;

public class tz160253_PackageOperations implements PackageOperations {
    EntityManager em = tz160253_GeneralOperations.em;

    @Override
    public int insertPackage(int addressFrom, int addressTo, String userName, int packageType, BigDecimal weight) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return -1;
        Korisnik user = resultListUser.get(0);
        
        Adresa addr1 = em.find(Adresa.class, addressFrom);
        if (addr1 == null)
            return -1;
        
        Adresa addr2 = em.find(Adresa.class, addressTo);
        if (addr2 == null)
            return -1;
        
        if (packageType != 0 && packageType != 1 && packageType != 2 && packageType != 3)
            return -1;
        
        Paket pack = new Paket();
        pack.setKorisnik(user);
        pack.setAdresaZaPreuzimanje(addr1);
        pack.setAdresaZaDostavljanje(addr2);
        pack.setTipPaketa(packageType);
        if (weight == null || weight.signum() < 0)
            pack.setTezinaPaketa(new BigDecimal(10)); //ako sam dobro shvatila
        else
            pack.setTezinaPaketa(weight);
        pack.setCena(BigDecimal.ZERO);
        pack.setStatusIsporuke(0);
        em.getTransaction().begin();
        em.persist(pack);
        em.getTransaction().commit();
        em.refresh(pack);
        return pack.getIdP();
    }

    @Override
    public boolean acceptAnOffer(int packageId) {
        Paket pack = em.find(Paket.class, packageId);
        if (pack == null)
            return false;
        
        if (pack.getStatusIsporuke() != 0)
            return false;
        
        Korisnik user = pack.getKorisnik();
          
        em.getTransaction().begin();
        pack.setVremePrihvatanjaZahteva(new java.util.Date());
        pack.setStatusIsporuke(1);
        pack.setTrenutnaLokacija(pack.getAdresaZaPreuzimanje());
        user.setBrojPoslatihPosiljki(user.getBrojPoslatihPosiljki() + 1);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean rejectAnOffer(int packageId) {
        Paket pack = em.find(Paket.class, packageId);
        if (pack == null)
            return false;
        
        if (pack.getStatusIsporuke() != 0)
            return false;
        
        em.getTransaction().begin();
        pack.setStatusIsporuke(4);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public List<Integer> getAllPackages() {
        return em.createQuery("SELECT p.idP FROM Paket p", Integer.class).getResultList();
    }

    @Override
    public List<Integer> getAllPackagesWithSpecificType(int type) {
        return em.createQuery("SELECT p.idP FROM Paket p WHERE p.tipPaketa = :tip", Integer.class).setParameter("tip", type).getResultList();
    }

    @Override
    public List<Integer> getAllUndeliveredPackages() {
        return em.createQuery("SELECT p.idP FROM Paket p WHERE p.statusIsporuke = 1 OR p.statusIsporuke = 2", Integer.class).getResultList();
    }

    @Override
    public List<Integer> getAllUndeliveredPackagesFromCity(int cityId) {
        return em.createQuery("SELECT p.idP FROM Paket p WHERE p.statusIsporuke = 1 OR p.statusIsporuke = 2 AND p.adresaZaPreuzimanje.idA IN (SELECT a.idA FROM Adresa a WHERE a.grad.idG = :idG)", Integer.class).setParameter("idG", cityId).getResultList();
    }

    @Override
    public List<Integer> getAllPackagesCurrentlyAtCity(int cityId) {
        return em.createQuery("SELECT p.idP FROM Paket p WHERE p.trenutnaLokacija.idA IN (SELECT a.idA FROM Adresa a WHERE a.grad.idG = :idG) AND p.idP NOT IN (SELECT pz.paket.idP FROM PrevoziSe pz WHERE pz.redniBroj<=0)", Integer.class).setParameter("idG", cityId).getResultList();
    }

    @Override
    public boolean deletePackage(int packageId) {
        Paket pack = em.find(Paket.class, packageId);
        if (pack == null)
            return false;
        if (pack.getStatusIsporuke() != 0 && pack.getStatusIsporuke() != 4)
            return false;
        
        em.getTransaction().begin();
        em.remove(pack);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean changeWeight(int packageId, BigDecimal newWeight) {
        Paket pack = em.find(Paket.class, packageId);
        if (pack == null)
            return false;
        if (pack.getStatusIsporuke() != 0)
            return false;
        
        em.getTransaction().begin();
        pack.setTezinaPaketa(newWeight);
        em.getTransaction().commit();
        em.refresh(pack);
        return true;
    }

    @Override
    public boolean changeType(int packageId, int newType) {
        Paket pack = em.find(Paket.class, packageId);
        if (pack == null)
            return false;
        if (newType != 0 && newType != 1 && newType != 2 && newType != 3)
            return false;
        if (pack.getStatusIsporuke() != 0)
            return false;
        
        em.getTransaction().begin();
        pack.setTipPaketa(newType);
        em.getTransaction().commit();
        em.refresh(pack);
        return true;
    }

    @Override
    public int getDeliveryStatus(int packageId) {
        Paket pack = em.find(Paket.class, packageId);
        if (pack == null)
            return -1; //ako ne postoji paket
        return pack.getStatusIsporuke();
    }

    @Override
    public BigDecimal getPriceOfDelivery(int packageId) {
        Paket pack = em.find(Paket.class, packageId);
        if (pack == null)
            return null; //ako ne postoji paket
        return pack.getCena();
    }

    @Override
    public int getCurrentLocationOfPackage(int packageId) {
        Paket pack = em.find(Paket.class, packageId);
        if (pack == null)
            return -1;
        if (pack.getStatusIsporuke() == 0 || pack.getStatusIsporuke() == 4)
            return -1;
        
        List<Integer> packagesInStockroom = em.createQuery("SELECT p.idP FROM Paket p WHERE p.idP = :idP AND p.idP NOT IN (SELECT pz.paket.idP FROM PrevoziSe pz WHERE pz.redniBroj<=0)", Integer.class).setParameter("idP", pack.getIdP()).getResultList();
        if (packagesInStockroom == null || packagesInStockroom.isEmpty())
            return -1;
        
        return pack.getTrenutnaLokacija().getIdA();
    }

    @Override
    public Date getAcceptanceTime(int packageId) {
        Paket pack = em.find(Paket.class, packageId);
        if (pack == null || pack.getStatusIsporuke() == 0 || pack.getStatusIsporuke() == 4)
            return null;
        
        return new Date(pack.getVremePrihvatanjaZahteva().getTime());
    }
    
}
