package rs.etf.sab.student;

import rs.etf.sab.student.entities.Korisnik;
import rs.etf.sab.student.entities.Kurir;
import rs.etf.sab.student.entities.ZahtevZaKurira;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import rs.etf.sab.operations.CourierRequestOperation;

public class tz160253_CourierRequestOperation implements CourierRequestOperation {
    EntityManager em = tz160253_GeneralOperations.em;

    @Override
    public boolean insertCourierRequest(String userName, String driverLicenceNumber) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return false;
        Korisnik user = resultListUser.get(0);
        
        Kurir courier = em.find(Kurir.class, user.getIdK());
        if (courier != null)
            return false;
        
        List<ZahtevZaKurira> resultListCourier = em.createNamedQuery("ZahtevZaKurira.findByBrojVozackeDozvole", ZahtevZaKurira.class).setParameter("brojVozackeDozvole", driverLicenceNumber).getResultList();
        if (resultListCourier != null && !resultListCourier.isEmpty())
            return false;
        
        ZahtevZaKurira request = em.find(ZahtevZaKurira.class, user.getIdK());
        if (request != null)
            return false;
        
        request = new ZahtevZaKurira();
        request.setKorisnik(user.getIdK());
        request.setBrojVozackeDozvole(driverLicenceNumber);
        em.getTransaction().begin();
        em.persist(request);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean deleteCourierRequest(String userName) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return false;
        
        ZahtevZaKurira request = em.find(ZahtevZaKurira.class, resultListUser.get(0).getIdK());
        if (request == null)
            return false;
        
        em.getTransaction().begin();
        em.remove(request);
        em.getTransaction().commit();
        
        return true;
    }

    @Override
    public boolean changeDriverLicenceNumberInCourierRequest(String userName, String licencePlateNumber) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return false;
        
        ZahtevZaKurira request = em.find(ZahtevZaKurira.class, resultListUser.get(0).getIdK());
        if (request == null)
            return false;
        
        List<ZahtevZaKurira> resultListCourier = em.createQuery("SELECT zk FROM ZahtevZaKurira zk WHERE zk.korisnik != :idK AND zk.brojVozackeDozvole = :brojVozackeDozvole", ZahtevZaKurira.class).setParameter("idK", request.getKorisnik()).setParameter("brojVozackeDozvole", licencePlateNumber).getResultList();
        if (resultListCourier != null && !resultListCourier.isEmpty())
            return false;
        
        em.getTransaction().begin();
        request.setBrojVozackeDozvole(licencePlateNumber);
        em.getTransaction().commit();
        
        return true;
    }

    @Override
    public List<String> getAllCourierRequests() {
        return em.createQuery("SELECT k.korisnickoIme FROM Korisnik k WHERE k.idK IN (SELECT z.korisnik FROM ZahtevZaKurira z)", String.class).getResultList();
    }

    @Override
    public boolean grantRequest(String username) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", username).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return false;
        Korisnik user = resultListUser.get(0);
        
        ZahtevZaKurira request = em.find(ZahtevZaKurira.class, user.getIdK());
        if (request == null)
            return false;
        
        List<Kurir> resultListCourier = em.createNamedQuery("Kurir.findByBrojVozackeDozvole", Kurir.class).setParameter("brojVozackeDozvole", request.getBrojVozackeDozvole()).getResultList();
        if (resultListCourier != null && !resultListCourier.isEmpty())
            return false;
        
        Kurir courier = new Kurir();
        courier.setIdK(user.getIdK());
        courier.setBrojVozackeDozvole(request.getBrojVozackeDozvole());
        courier.setBrojIsporucenihPaketa(0);
        courier.setOstvarenProfit(BigDecimal.ZERO);
        courier.setStatus(0);
        em.getTransaction().begin();
        em.remove(request);
        em.persist(courier);
        em.getTransaction().commit();
        
        return true;
    }
    
}
