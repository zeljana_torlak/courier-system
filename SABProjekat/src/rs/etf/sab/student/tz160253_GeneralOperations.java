package rs.etf.sab.student;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import rs.etf.sab.operations.GeneralOperations;

public class tz160253_GeneralOperations implements GeneralOperations {
    static EntityManager em = Persistence.createEntityManagerFactory("SABProjekatPU").createEntityManager();

    @Override
    public void eraseAll() {
        em.getTransaction().begin();
        
        em.createNativeQuery("EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL' ").executeUpdate();
        em.createNativeQuery("EXEC sp_MSForEachTable 'DELETE FROM ?'").executeUpdate();
        em.createNativeQuery("EXEC sp_MSForEachTable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'").executeUpdate();
        
        em.getTransaction().commit();
    }
    
}
