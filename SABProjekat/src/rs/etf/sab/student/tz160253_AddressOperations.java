package rs.etf.sab.student;

import rs.etf.sab.student.entities.Adresa;
import rs.etf.sab.student.entities.Grad;
import java.util.List;
import javax.persistence.EntityManager;
import rs.etf.sab.operations.AddressOperations;

public class tz160253_AddressOperations implements AddressOperations {
    EntityManager em = tz160253_GeneralOperations.em;

    @Override
    public int insertAddress(String street, int number, int cityId, int xCord, int yCord) {
        Grad city = em.find(Grad.class, cityId);
        if (city == null)
            return -1;
        
        Adresa address = new Adresa();
        address.setUlica(street);
        address.setBroj(number);
        address.setGrad(city);
        address.setXkm(xCord);
        address.setYkm(yCord);
        em.getTransaction().begin();
        em.persist(address);
        em.getTransaction().commit();
        return address.getIdA();
    }

    @Override
    public int deleteAddresses(String name, int number) {
        int counter = 0;
        
        List<Adresa> resultList = em.createQuery("SELECT a FROM Adresa a WHERE a.ulica = :ulica AND a.broj = :broj", Adresa.class).setParameter("ulica", name).setParameter("broj", number).getResultList();
        if (resultList == null || resultList.isEmpty())
            return 0;
        
        em.getTransaction().begin();
        for (int i = 0; i < resultList.size(); i++)
            em.remove(resultList.get(i));
        counter += resultList.size();
        em.getTransaction().commit();
        
        return counter;
    }

    @Override
    public boolean deleteAdress(int idAddress) {
        Adresa address = em.find(Adresa.class, idAddress);
        if (address == null)
            return false;
        
        em.getTransaction().begin();
        em.remove(address);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public int deleteAllAddressesFromCity(int idCity) {
        int counter = 0;
        
        List<Adresa> resultList = em.createQuery("SELECT a FROM Adresa a WHERE a.grad.idG = :idG", Adresa.class).setParameter("idG", idCity).getResultList();
        if (resultList == null || resultList.isEmpty())
            return 0;
        
        em.getTransaction().begin();
        for (int i = 0; i < resultList.size(); i++)
            em.remove(resultList.get(i));
        counter += resultList.size();
        em.getTransaction().commit();
        
        return counter;
    }
    
    @Override
    public List<Integer> getAllAddresses() {
        return em.createQuery("SELECT a.idA FROM Adresa a", Integer.class).getResultList();
    }

    @Override
    public List<Integer> getAllAddressesFromCity(int idCity) {
        Grad city = em.find(Grad.class, idCity);
        if (city == null)
            return null;
        return em.createQuery("SELECT a.idA FROM Adresa a WHERE a.grad.idG = :idG", Integer.class).setParameter("idG", idCity).getResultList();
    }
    
}
