package rs.etf.sab.student;

import rs.etf.sab.student.entities.Administrator;
import rs.etf.sab.student.entities.Adresa;
import rs.etf.sab.student.entities.Korisnik;
import java.util.List;
import javax.persistence.EntityManager;
import rs.etf.sab.operations.UserOperations;

public class tz160253_UserOperations implements UserOperations {
    EntityManager em = tz160253_GeneralOperations.em;

    @Override
    public boolean insertUser(String userName, String firstName, String lastName, String password, int idAddress) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getResultList();
        if (resultListUser != null && !resultListUser.isEmpty())
            return false;
        
        if (firstName.equals("") || lastName.equals(""))
            return false; //mozda nije neophodno
        if (!(Character.isUpperCase(firstName.codePointAt(0)) && Character.isUpperCase(lastName.codePointAt(0))))
            return false; //charAt(0)
        
        if (!password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_\\.@#$%^&+=])(?=\\S+$).{8,}$"))
            return false;
          
        Adresa address = em.find(Adresa.class, idAddress);
        if (address == null)
            return false;
        
        Korisnik user = new Korisnik();
        user.setIme(firstName);
        user.setPrezime(lastName);
        user.setKorisnickoIme(userName);
        user.setSifra(password);
        user.setAdresa(address);
        user.setBrojPoslatihPosiljki(0);
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean declareAdmin(String userName) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return false;
        Korisnik user = resultListUser.get(0);
        
        Administrator adminUser = em.find(Administrator.class, user.getIdK());
        if (adminUser != null)
            return false;
        
        adminUser = new Administrator();
        adminUser.setIdK(user.getIdK());
        em.getTransaction().begin();
        em.persist(adminUser);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public int getSentPackages(String... userNames) {
        int counter = 0;
        
        boolean exists = false;
        for (String name: userNames){
            List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", name).getResultList();
            if (resultListUser == null || resultListUser.isEmpty())
                continue;
            exists = true;
            counter += resultListUser.get(0).getBrojPoslatihPosiljki();
        }
        
        if (!exists)
            return -1;
        return counter;
    }

    @Override
    public int deleteUsers(String... userNames) {
        int counter = 0;
        
        em.getTransaction().begin();
        for (String name: userNames){
            List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", name).getResultList();
            if (resultListUser == null || resultListUser.isEmpty())
                continue;
            em.remove(resultListUser.get(0));
            counter++;
        }
        em.getTransaction().commit();
        
        return counter;
    }

    @Override
    public List<String> getAllUsers() {
        return em.createQuery("SELECT k.korisnickoIme FROM Korisnik k", String.class).getResultList();
    }
    
}
