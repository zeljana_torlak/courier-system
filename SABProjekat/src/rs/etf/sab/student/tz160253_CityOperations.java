package rs.etf.sab.student;

import rs.etf.sab.student.entities.Grad;
import java.util.List;
import javax.persistence.EntityManager;
import rs.etf.sab.operations.CityOperations;

public class tz160253_CityOperations implements CityOperations {
    EntityManager em = tz160253_GeneralOperations.em;

    @Override
    public int insertCity(String name, String postalCode) {
        List<Grad> resultList = em.createNamedQuery("Grad.findByPostanskiBroj", Grad.class).setParameter("postanskiBroj", postalCode).getResultList();
        if (resultList != null && !resultList.isEmpty())
            return -1;
        
        Grad city = new Grad();
        city.setNaziv(name);
        city.setPostanskiBroj(postalCode);
        em.getTransaction().begin();
        em.persist(city);
        em.getTransaction().commit();
        return city.getIdG();
    }

    @Override
    public int deleteCity(String... names) {
        int counter = 0;
        
        em.getTransaction().begin();
        for (String name: names){
            List<Grad> resultList = em.createNamedQuery("Grad.findByNaziv", Grad.class).setParameter("naziv", name).getResultList();
            if (resultList == null || resultList.isEmpty())
                continue;
            for (int i = 0; i < resultList.size(); i++)
                em.remove(resultList.get(i));
            counter += resultList.size();
        }
        em.getTransaction().commit();
        
        return counter;
    }

    @Override
    public boolean deleteCity(int idCity) {
        Grad city = em.find(Grad.class, idCity);
        if (city == null)
            return false;
        
        em.getTransaction().begin();
        em.remove(city);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public List<Integer> getAllCities() {
        return em.createQuery("SELECT g.idG FROM Grad g", Integer.class).getResultList();
    }
    
}
