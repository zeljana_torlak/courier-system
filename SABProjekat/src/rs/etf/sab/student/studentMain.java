package rs.etf.sab.student;

import rs.etf.sab.operations.*;
import rs.etf.sab.tests.TestHandler;
import rs.etf.sab.tests.TestRunner;


public class studentMain {

    public static void main(String[] args) {
        AddressOperations addressOperations = new tz160253_AddressOperations(); // Change this to your implementation.
        CityOperations cityOperations = new tz160253_CityOperations(); // Do it for all classes.
        CourierOperations courierOperations = new tz160253_CourierOperations(); // e.g. = new MyDistrictOperations();
        CourierRequestOperation courierRequestOperation = new tz160253_CourierRequestOperation();
        DriveOperation driveOperation = new tz160253_DriveOperation();
        GeneralOperations generalOperations = new tz160253_GeneralOperations();
        PackageOperations packageOperations = new tz160253_PackageOperations();
        StockroomOperations stockroomOperations = new tz160253_StockroomOperations();
        UserOperations userOperations = new tz160253_UserOperations();
        VehicleOperations vehicleOperations = new tz160253_VehicleOperations();


        TestHandler.createInstance(
                addressOperations,
                cityOperations,
                courierOperations,
                courierRequestOperation,
                driveOperation,
                generalOperations,
                packageOperations,
                stockroomOperations,
                userOperations,
                vehicleOperations);

        TestRunner.runTests();
    }
}
