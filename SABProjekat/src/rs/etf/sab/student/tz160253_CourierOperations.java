package rs.etf.sab.student;

import rs.etf.sab.student.entities.Korisnik;
import rs.etf.sab.student.entities.Kurir;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import rs.etf.sab.operations.CourierOperations;

public class tz160253_CourierOperations implements CourierOperations {
    EntityManager em = tz160253_GeneralOperations.em;

    @Override
    public boolean insertCourier(String courierUserName, String driverLicenceNumber) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUserName).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return false;
        Korisnik user = resultListUser.get(0);
        
        List<Kurir> resultListCourier = em.createNamedQuery("Kurir.findByBrojVozackeDozvole", Kurir.class).setParameter("brojVozackeDozvole", driverLicenceNumber).getResultList();
        if (resultListCourier != null && !resultListCourier.isEmpty())
            return false;
        
        Kurir courier = em.find(Kurir.class, user.getIdK());
        if (courier != null)
            return false;
        
        courier = new Kurir();
        courier.setIdK(user.getIdK());
        courier.setBrojVozackeDozvole(driverLicenceNumber);
        courier.setBrojIsporucenihPaketa(0);
        courier.setOstvarenProfit(BigDecimal.ZERO);
        courier.setStatus(0);
        em.getTransaction().begin();
        em.persist(courier);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean deleteCourier(String courierUserName) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUserName).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return false;
        
        Kurir courier = em.find(Kurir.class, resultListUser.get(0).getIdK());
        if (courier == null)
            return false;
        
        em.getTransaction().begin();
        em.remove(courier);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public List<String> getCouriersWithStatus(int statusOfCourier) {
         return em.createQuery("SELECT k.korisnickoIme FROM Korisnik k WHERE k.idK IN (SELECT kr.idK FROM Kurir kr WHERE kr.status = :statusOfCourier)", String.class).setParameter("statusOfCourier", statusOfCourier).getResultList();
    }

    @Override
    public List<String> getAllCouriers() { //mora spajanje tabela
        return em.createQuery("SELECT k.korisnickoIme FROM Korisnik k, Kurir kr WHERE k.idK = kr.idK ORDER BY kr.ostvarenProfit DESC", String.class).getResultList();
    }

    @Override
    public BigDecimal getAverageCourierProfit(int numberOfDeliveries) {
        if (numberOfDeliveries == -1)
            return em.createQuery("SELECT COALESCE(AVG(k.ostvarenProfit), 0) FROM Kurir k", BigDecimal.class).getResultList().get(0);
        
        return em.createQuery("SELECT COALESCE(AVG(k.ostvarenProfit), 0) FROM Kurir k WHERE k.brojIsporucenihPaketa = :broj", BigDecimal.class).setParameter("broj", numberOfDeliveries).getResultList().get(0);
    }
    
}
