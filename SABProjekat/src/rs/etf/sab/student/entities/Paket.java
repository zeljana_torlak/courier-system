/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "Paket")
@NamedQueries({
    @NamedQuery(name = "Paket.findAll", query = "SELECT p FROM Paket p"),
    @NamedQuery(name = "Paket.findByTipPaketa", query = "SELECT p FROM Paket p WHERE p.tipPaketa = :tipPaketa"),
    @NamedQuery(name = "Paket.findByTezinaPaketa", query = "SELECT p FROM Paket p WHERE p.tezinaPaketa = :tezinaPaketa"),
    @NamedQuery(name = "Paket.findByIdP", query = "SELECT p FROM Paket p WHERE p.idP = :idP"),
    @NamedQuery(name = "Paket.findByStatusIsporuke", query = "SELECT p FROM Paket p WHERE p.statusIsporuke = :statusIsporuke"),
    @NamedQuery(name = "Paket.findByCena", query = "SELECT p FROM Paket p WHERE p.cena = :cena"),
    @NamedQuery(name = "Paket.findByVremeKreiranjaZahteva", query = "SELECT p FROM Paket p WHERE p.vremeKreiranjaZahteva = :vremeKreiranjaZahteva"),
    @NamedQuery(name = "Paket.findByVremePrihvatanjaZahteva", query = "SELECT p FROM Paket p WHERE p.vremePrihvatanjaZahteva = :vremePrihvatanjaZahteva")})
public class Paket implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "TipPaketa")
    private int tipPaketa;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "TezinaPaketa")
    private BigDecimal tezinaPaketa;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdP")
    private Integer idP;
    @Basic(optional = false)
    @Column(name = "StatusIsporuke")
    private int statusIsporuke;
    @Basic(optional = false)
    @Column(name = "Cena")
    private BigDecimal cena;
    @Column(name = "VremeKreiranjaZahteva")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vremeKreiranjaZahteva;
    @Column(name = "VremePrihvatanjaZahteva")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vremePrihvatanjaZahteva;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paket")
    private List<PrevoziSe> prevoziSeList;
    @JoinColumn(name = "AdresaZaPreuzimanje", referencedColumnName = "IdA")
    @ManyToOne(optional = false)
    private Adresa adresaZaPreuzimanje;
    @JoinColumn(name = "AdresaZaDostavljanje", referencedColumnName = "IdA")
    @ManyToOne(optional = false)
    private Adresa adresaZaDostavljanje;
    @JoinColumn(name = "TrenutnaLokacija", referencedColumnName = "IdA")
    @ManyToOne
    private Adresa trenutnaLokacija;
    @JoinColumn(name = "Korisnik", referencedColumnName = "IdK")
    @ManyToOne(optional = false)
    private Korisnik korisnik;

    public Paket() {
    }

    public Paket(Integer idP) {
        this.idP = idP;
    }

    public Paket(Integer idP, int tipPaketa, BigDecimal tezinaPaketa, int statusIsporuke, BigDecimal cena) {
        this.idP = idP;
        this.tipPaketa = tipPaketa;
        this.tezinaPaketa = tezinaPaketa;
        this.statusIsporuke = statusIsporuke;
        this.cena = cena;
    }

    public int getTipPaketa() {
        return tipPaketa;
    }

    public void setTipPaketa(int tipPaketa) {
        this.tipPaketa = tipPaketa;
    }

    public BigDecimal getTezinaPaketa() {
        return tezinaPaketa;
    }

    public void setTezinaPaketa(BigDecimal tezinaPaketa) {
        this.tezinaPaketa = tezinaPaketa;
    }

    public Integer getIdP() {
        return idP;
    }

    public void setIdP(Integer idP) {
        this.idP = idP;
    }

    public int getStatusIsporuke() {
        return statusIsporuke;
    }

    public void setStatusIsporuke(int statusIsporuke) {
        this.statusIsporuke = statusIsporuke;
    }

    public BigDecimal getCena() {
        return cena;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public Date getVremeKreiranjaZahteva() {
        return vremeKreiranjaZahteva;
    }

    public void setVremeKreiranjaZahteva(Date vremeKreiranjaZahteva) {
        this.vremeKreiranjaZahteva = vremeKreiranjaZahteva;
    }

    public Date getVremePrihvatanjaZahteva() {
        return vremePrihvatanjaZahteva;
    }

    public void setVremePrihvatanjaZahteva(Date vremePrihvatanjaZahteva) {
        this.vremePrihvatanjaZahteva = vremePrihvatanjaZahteva;
    }

    public List<PrevoziSe> getPrevoziSeList() {
        return prevoziSeList;
    }

    public void setPrevoziSeList(List<PrevoziSe> prevoziSeList) {
        this.prevoziSeList = prevoziSeList;
    }

    public Adresa getAdresaZaPreuzimanje() {
        return adresaZaPreuzimanje;
    }

    public void setAdresaZaPreuzimanje(Adresa adresaZaPreuzimanje) {
        this.adresaZaPreuzimanje = adresaZaPreuzimanje;
    }

    public Adresa getAdresaZaDostavljanje() {
        return adresaZaDostavljanje;
    }

    public void setAdresaZaDostavljanje(Adresa adresaZaDostavljanje) {
        this.adresaZaDostavljanje = adresaZaDostavljanje;
    }

    public Adresa getTrenutnaLokacija() {
        return trenutnaLokacija;
    }

    public void setTrenutnaLokacija(Adresa trenutnaLokacija) {
        this.trenutnaLokacija = trenutnaLokacija;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idP != null ? idP.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paket)) {
            return false;
        }
        Paket other = (Paket) object;
        if ((this.idP == null && other.idP != null) || (this.idP != null && !this.idP.equals(other.idP))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Paket[ idP=" + idP + " ]";
    }
    
}
