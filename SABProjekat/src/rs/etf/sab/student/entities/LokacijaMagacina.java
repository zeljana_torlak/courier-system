/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "LokacijaMagacina")
@NamedQueries({
    @NamedQuery(name = "LokacijaMagacina.findAll", query = "SELECT l FROM LokacijaMagacina l"),
    @NamedQuery(name = "LokacijaMagacina.findByIdLM", query = "SELECT l FROM LokacijaMagacina l WHERE l.idLM = :idLM")})
public class LokacijaMagacina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdLM")
    private Integer idLM;
    @JoinColumn(name = "Adresa", referencedColumnName = "IdA")
    @ManyToOne(optional = false)
    private Adresa adresa;

    public LokacijaMagacina() {
    }

    public LokacijaMagacina(Integer idLM) {
        this.idLM = idLM;
    }

    public Integer getIdLM() {
        return idLM;
    }

    public void setIdLM(Integer idLM) {
        this.idLM = idLM;
    }

    public Adresa getAdresa() {
        return adresa;
    }

    public void setAdresa(Adresa adresa) {
        this.adresa = adresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLM != null ? idLM.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LokacijaMagacina)) {
            return false;
        }
        LokacijaMagacina other = (LokacijaMagacina) object;
        if ((this.idLM == null && other.idLM != null) || (this.idLM != null && !this.idLM.equals(other.idLM))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.LokacijaMagacina[ idLM=" + idLM + " ]";
    }
    
}
