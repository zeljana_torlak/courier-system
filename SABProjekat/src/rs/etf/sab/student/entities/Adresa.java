/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "Adresa")
@NamedQueries({
    @NamedQuery(name = "Adresa.findAll", query = "SELECT a FROM Adresa a"),
    @NamedQuery(name = "Adresa.findByIdA", query = "SELECT a FROM Adresa a WHERE a.idA = :idA"),
    @NamedQuery(name = "Adresa.findByUlica", query = "SELECT a FROM Adresa a WHERE a.ulica = :ulica"),
    @NamedQuery(name = "Adresa.findByBroj", query = "SELECT a FROM Adresa a WHERE a.broj = :broj"),
    @NamedQuery(name = "Adresa.findByXkm", query = "SELECT a FROM Adresa a WHERE a.xkm = :xkm"),
    @NamedQuery(name = "Adresa.findByYkm", query = "SELECT a FROM Adresa a WHERE a.ykm = :ykm")})
public class Adresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdA")
    private Integer idA;
    @Basic(optional = false)
    @Column(name = "Ulica")
    private String ulica;
    @Basic(optional = false)
    @Column(name = "Broj")
    private int broj;
    @Basic(optional = false)
    @Column(name = "X_km")
    private int xkm;
    @Basic(optional = false)
    @Column(name = "Y_km")
    private int ykm;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trenutnaLokacija")
    private List<Vozi> voziList;
    @JoinColumn(name = "Grad", referencedColumnName = "IdG")
    @ManyToOne(optional = false)
    private Grad grad;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adresa")
    private List<Korisnik> korisnikList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adresaZaPreuzimanje")
    private List<Paket> paketList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adresaZaDostavljanje")
    private List<Paket> paketList1;
    @OneToMany(mappedBy = "trenutnaLokacija")
    private List<Paket> paketList2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adresa")
    private List<LokacijaMagacina> lokacijaMagacinaList;
    @OneToMany(mappedBy = "lokacijaParkirano")
    private List<Vozilo> voziloList;

    public Adresa() {
    }

    public Adresa(Integer idA) {
        this.idA = idA;
    }

    public Adresa(Integer idA, String ulica, int broj, int xkm, int ykm) {
        this.idA = idA;
        this.ulica = ulica;
        this.broj = broj;
        this.xkm = xkm;
        this.ykm = ykm;
    }

    public Integer getIdA() {
        return idA;
    }

    public void setIdA(Integer idA) {
        this.idA = idA;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public int getBroj() {
        return broj;
    }

    public void setBroj(int broj) {
        this.broj = broj;
    }

    public int getXkm() {
        return xkm;
    }

    public void setXkm(int xkm) {
        this.xkm = xkm;
    }

    public int getYkm() {
        return ykm;
    }

    public void setYkm(int ykm) {
        this.ykm = ykm;
    }

    public List<Vozi> getVoziList() {
        return voziList;
    }

    public void setVoziList(List<Vozi> voziList) {
        this.voziList = voziList;
    }

    public Grad getGrad() {
        return grad;
    }

    public void setGrad(Grad grad) {
        this.grad = grad;
    }

    public List<Korisnik> getKorisnikList() {
        return korisnikList;
    }

    public void setKorisnikList(List<Korisnik> korisnikList) {
        this.korisnikList = korisnikList;
    }

    public List<Paket> getPaketList() {
        return paketList;
    }

    public void setPaketList(List<Paket> paketList) {
        this.paketList = paketList;
    }

    public List<Paket> getPaketList1() {
        return paketList1;
    }

    public void setPaketList1(List<Paket> paketList1) {
        this.paketList1 = paketList1;
    }

    public List<Paket> getPaketList2() {
        return paketList2;
    }

    public void setPaketList2(List<Paket> paketList2) {
        this.paketList2 = paketList2;
    }

    public List<LokacijaMagacina> getLokacijaMagacinaList() {
        return lokacijaMagacinaList;
    }

    public void setLokacijaMagacinaList(List<LokacijaMagacina> lokacijaMagacinaList) {
        this.lokacijaMagacinaList = lokacijaMagacinaList;
    }

    public List<Vozilo> getVoziloList() {
        return voziloList;
    }

    public void setVoziloList(List<Vozilo> voziloList) {
        this.voziloList = voziloList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idA != null ? idA.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adresa)) {
            return false;
        }
        Adresa other = (Adresa) object;
        if ((this.idA == null && other.idA != null) || (this.idA != null && !this.idA.equals(other.idA))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Adresa[ idA=" + idA + " ]";
    }
    
}
