/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "ZahtevZaKurira")
@NamedQueries({
    @NamedQuery(name = "ZahtevZaKurira.findAll", query = "SELECT z FROM ZahtevZaKurira z"),
    @NamedQuery(name = "ZahtevZaKurira.findByBrojVozackeDozvole", query = "SELECT z FROM ZahtevZaKurira z WHERE z.brojVozackeDozvole = :brojVozackeDozvole"),
    @NamedQuery(name = "ZahtevZaKurira.findByKorisnik", query = "SELECT z FROM ZahtevZaKurira z WHERE z.korisnik = :korisnik")})
public class ZahtevZaKurira implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "BrojVozackeDozvole")
    private String brojVozackeDozvole;
    @Id
    @Basic(optional = false)
    @Column(name = "Korisnik")
    private Integer korisnik;
    @JoinColumn(name = "Korisnik", referencedColumnName = "IdK", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Korisnik korisnik1;

    public ZahtevZaKurira() {
    }

    public ZahtevZaKurira(Integer korisnik) {
        this.korisnik = korisnik;
    }

    public ZahtevZaKurira(Integer korisnik, String brojVozackeDozvole) {
        this.korisnik = korisnik;
        this.brojVozackeDozvole = brojVozackeDozvole;
    }

    public String getBrojVozackeDozvole() {
        return brojVozackeDozvole;
    }

    public void setBrojVozackeDozvole(String brojVozackeDozvole) {
        this.brojVozackeDozvole = brojVozackeDozvole;
    }

    public Integer getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Integer korisnik) {
        this.korisnik = korisnik;
    }

    public Korisnik getKorisnik1() {
        return korisnik1;
    }

    public void setKorisnik1(Korisnik korisnik1) {
        this.korisnik1 = korisnik1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (korisnik != null ? korisnik.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ZahtevZaKurira)) {
            return false;
        }
        ZahtevZaKurira other = (ZahtevZaKurira) object;
        if ((this.korisnik == null && other.korisnik != null) || (this.korisnik != null && !this.korisnik.equals(other.korisnik))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ZahtevZaKurira[ korisnik=" + korisnik + " ]";
    }
    
}
