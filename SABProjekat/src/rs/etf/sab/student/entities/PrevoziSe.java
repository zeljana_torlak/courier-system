/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "PrevoziSe")
@NamedQueries({
    @NamedQuery(name = "PrevoziSe.findAll", query = "SELECT p FROM PrevoziSe p"),
    @NamedQuery(name = "PrevoziSe.findByIdPrevoziSe", query = "SELECT p FROM PrevoziSe p WHERE p.idPrevoziSe = :idPrevoziSe"),
    @NamedQuery(name = "PrevoziSe.findByRedniBroj", query = "SELECT p FROM PrevoziSe p WHERE p.redniBroj = :redniBroj")})
public class PrevoziSe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdPrevoziSe")
    private Integer idPrevoziSe;
    @Basic(optional = false)
    @Column(name = "RedniBroj")
    private int redniBroj;
    @JoinColumn(name = "Paket", referencedColumnName = "IdP")
    @ManyToOne(optional = false)
    private Paket paket;
    @JoinColumn(name = "Vozi", referencedColumnName = "IdVozi")
    @ManyToOne(optional = false)
    private Vozi vozi;

    public PrevoziSe() {
    }

    public PrevoziSe(Integer idPrevoziSe) {
        this.idPrevoziSe = idPrevoziSe;
    }

    public PrevoziSe(Integer idPrevoziSe, int redniBroj) {
        this.idPrevoziSe = idPrevoziSe;
        this.redniBroj = redniBroj;
    }

    public Integer getIdPrevoziSe() {
        return idPrevoziSe;
    }

    public void setIdPrevoziSe(Integer idPrevoziSe) {
        this.idPrevoziSe = idPrevoziSe;
    }

    public int getRedniBroj() {
        return redniBroj;
    }

    public void setRedniBroj(int redniBroj) {
        this.redniBroj = redniBroj;
    }

    public Paket getPaket() {
        return paket;
    }

    public void setPaket(Paket paket) {
        this.paket = paket;
    }

    public Vozi getVozi() {
        return vozi;
    }

    public void setVozi(Vozi vozi) {
        this.vozi = vozi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPrevoziSe != null ? idPrevoziSe.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrevoziSe)) {
            return false;
        }
        PrevoziSe other = (PrevoziSe) object;
        if ((this.idPrevoziSe == null && other.idPrevoziSe != null) || (this.idPrevoziSe != null && !this.idPrevoziSe.equals(other.idPrevoziSe))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PrevoziSe[ idPrevoziSe=" + idPrevoziSe + " ]";
    }
    
}
