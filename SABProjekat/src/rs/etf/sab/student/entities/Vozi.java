/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "Vozi")
@NamedQueries({
    @NamedQuery(name = "Vozi.findAll", query = "SELECT v FROM Vozi v"),
    @NamedQuery(name = "Vozi.findByIdVozi", query = "SELECT v FROM Vozi v WHERE v.idVozi = :idVozi"),
    @NamedQuery(name = "Vozi.findByTrenutniProfit", query = "SELECT v FROM Vozi v WHERE v.trenutniProfit = :trenutniProfit"),
    @NamedQuery(name = "Vozi.findByOpterecenost", query = "SELECT v FROM Vozi v WHERE v.opterecenost = :opterecenost")})
public class Vozi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdVozi")
    private Integer idVozi;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "TrenutniProfit")
    private BigDecimal trenutniProfit;
    @Basic(optional = false)
    @Column(name = "Opterecenost")
    private BigDecimal opterecenost;
    @JoinColumn(name = "TrenutnaLokacija", referencedColumnName = "IdA")
    @ManyToOne(optional = false)
    private Adresa trenutnaLokacija;
    @JoinColumn(name = "Kurir", referencedColumnName = "IdK")
    @ManyToOne(optional = false)
    private Kurir kurir;
    @JoinColumn(name = "Vozilo", referencedColumnName = "IdV")
    @ManyToOne(optional = false)
    private Vozilo vozilo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vozi")
    private List<PrevoziSe> prevoziSeList;

    public Vozi() {
    }

    public Vozi(Integer idVozi) {
        this.idVozi = idVozi;
    }

    public Vozi(Integer idVozi, BigDecimal trenutniProfit, BigDecimal opterecenost) {
        this.idVozi = idVozi;
        this.trenutniProfit = trenutniProfit;
        this.opterecenost = opterecenost;
    }

    public Integer getIdVozi() {
        return idVozi;
    }

    public void setIdVozi(Integer idVozi) {
        this.idVozi = idVozi;
    }

    public BigDecimal getTrenutniProfit() {
        return trenutniProfit;
    }

    public void setTrenutniProfit(BigDecimal trenutniProfit) {
        this.trenutniProfit = trenutniProfit;
    }

    public BigDecimal getOpterecenost() {
        return opterecenost;
    }

    public void setOpterecenost(BigDecimal opterecenost) {
        this.opterecenost = opterecenost;
    }

    public Adresa getTrenutnaLokacija() {
        return trenutnaLokacija;
    }

    public void setTrenutnaLokacija(Adresa trenutnaLokacija) {
        this.trenutnaLokacija = trenutnaLokacija;
    }

    public Kurir getKurir() {
        return kurir;
    }

    public void setKurir(Kurir kurir) {
        this.kurir = kurir;
    }

    public Vozilo getVozilo() {
        return vozilo;
    }

    public void setVozilo(Vozilo vozilo) {
        this.vozilo = vozilo;
    }

    public List<PrevoziSe> getPrevoziSeList() {
        return prevoziSeList;
    }

    public void setPrevoziSeList(List<PrevoziSe> prevoziSeList) {
        this.prevoziSeList = prevoziSeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVozi != null ? idVozi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vozi)) {
            return false;
        }
        Vozi other = (Vozi) object;
        if ((this.idVozi == null && other.idVozi != null) || (this.idVozi != null && !this.idVozi.equals(other.idVozi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Vozi[ idVozi=" + idVozi + " ]";
    }
    
}
