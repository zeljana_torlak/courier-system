/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "Kurir")
@NamedQueries({
    @NamedQuery(name = "Kurir.findAll", query = "SELECT k FROM Kurir k"),
    @NamedQuery(name = "Kurir.findByBrojIsporucenihPaketa", query = "SELECT k FROM Kurir k WHERE k.brojIsporucenihPaketa = :brojIsporucenihPaketa"),
    @NamedQuery(name = "Kurir.findByOstvarenProfit", query = "SELECT k FROM Kurir k WHERE k.ostvarenProfit = :ostvarenProfit"),
    @NamedQuery(name = "Kurir.findByStatus", query = "SELECT k FROM Kurir k WHERE k.status = :status"),
    @NamedQuery(name = "Kurir.findByBrojVozackeDozvole", query = "SELECT k FROM Kurir k WHERE k.brojVozackeDozvole = :brojVozackeDozvole"),
    @NamedQuery(name = "Kurir.findByIdK", query = "SELECT k FROM Kurir k WHERE k.idK = :idK")})
public class Kurir implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "BrojIsporucenihPaketa")
    private int brojIsporucenihPaketa;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "OstvarenProfit")
    private BigDecimal ostvarenProfit;
    @Basic(optional = false)
    @Column(name = "Status")
    private int status;
    @Basic(optional = false)
    @Column(name = "BrojVozackeDozvole")
    private String brojVozackeDozvole;
    @Id
    @Basic(optional = false)
    @Column(name = "IdK")
    private Integer idK;
    @JoinTable(name = "Vozio", joinColumns = {
        @JoinColumn(name = "Kurir", referencedColumnName = "IdK")}, inverseJoinColumns = {
        @JoinColumn(name = "Vozilo", referencedColumnName = "IdV")})
    @ManyToMany
    private List<Vozilo> voziloList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kurir")
    private List<Vozi> voziList;
    @JoinColumn(name = "IdK", referencedColumnName = "IdK", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Korisnik korisnik;

    public Kurir() {
    }

    public Kurir(Integer idK) {
        this.idK = idK;
    }

    public Kurir(Integer idK, int brojIsporucenihPaketa, BigDecimal ostvarenProfit, int status, String brojVozackeDozvole) {
        this.idK = idK;
        this.brojIsporucenihPaketa = brojIsporucenihPaketa;
        this.ostvarenProfit = ostvarenProfit;
        this.status = status;
        this.brojVozackeDozvole = brojVozackeDozvole;
    }

    public int getBrojIsporucenihPaketa() {
        return brojIsporucenihPaketa;
    }

    public void setBrojIsporucenihPaketa(int brojIsporucenihPaketa) {
        this.brojIsporucenihPaketa = brojIsporucenihPaketa;
    }

    public BigDecimal getOstvarenProfit() {
        return ostvarenProfit;
    }

    public void setOstvarenProfit(BigDecimal ostvarenProfit) {
        this.ostvarenProfit = ostvarenProfit;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getBrojVozackeDozvole() {
        return brojVozackeDozvole;
    }

    public void setBrojVozackeDozvole(String brojVozackeDozvole) {
        this.brojVozackeDozvole = brojVozackeDozvole;
    }

    public Integer getIdK() {
        return idK;
    }

    public void setIdK(Integer idK) {
        this.idK = idK;
    }

    public List<Vozilo> getVoziloList() {
        return voziloList;
    }

    public void setVoziloList(List<Vozilo> voziloList) {
        this.voziloList = voziloList;
    }

    public List<Vozi> getVoziList() {
        return voziList;
    }

    public void setVoziList(List<Vozi> voziList) {
        this.voziList = voziList;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idK != null ? idK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kurir)) {
            return false;
        }
        Kurir other = (Kurir) object;
        if ((this.idK == null && other.idK != null) || (this.idK != null && !this.idK.equals(other.idK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Kurir[ idK=" + idK + " ]";
    }
    
}
