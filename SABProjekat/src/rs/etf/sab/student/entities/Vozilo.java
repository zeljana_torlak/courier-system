/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "Vozilo")
@NamedQueries({
    @NamedQuery(name = "Vozilo.findAll", query = "SELECT v FROM Vozilo v"),
    @NamedQuery(name = "Vozilo.findByRegistracioniBroj", query = "SELECT v FROM Vozilo v WHERE v.registracioniBroj = :registracioniBroj"),
    @NamedQuery(name = "Vozilo.findByTipGoriva", query = "SELECT v FROM Vozilo v WHERE v.tipGoriva = :tipGoriva"),
    @NamedQuery(name = "Vozilo.findByPotrosnjalperkm", query = "SELECT v FROM Vozilo v WHERE v.potrosnjalperkm = :potrosnjalperkm"),
    @NamedQuery(name = "Vozilo.findByNosivost", query = "SELECT v FROM Vozilo v WHERE v.nosivost = :nosivost"),
    @NamedQuery(name = "Vozilo.findByIdV", query = "SELECT v FROM Vozilo v WHERE v.idV = :idV")})
public class Vozilo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "RegistracioniBroj")
    private String registracioniBroj;
    @Basic(optional = false)
    @Column(name = "TipGoriva")
    private int tipGoriva;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "Potrosnja_l_per_km")
    private BigDecimal potrosnjalperkm;
    @Basic(optional = false)
    @Column(name = "Nosivost")
    private BigDecimal nosivost;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdV")
    private Integer idV;
    @ManyToMany(mappedBy = "voziloList")
    private List<Kurir> kurirList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vozilo")
    private List<Vozi> voziList;
    @JoinColumn(name = "LokacijaParkirano", referencedColumnName = "IdA")
    @ManyToOne
    private Adresa lokacijaParkirano;

    public Vozilo() {
    }

    public Vozilo(Integer idV) {
        this.idV = idV;
    }

    public Vozilo(Integer idV, String registracioniBroj, int tipGoriva, BigDecimal potrosnjalperkm, BigDecimal nosivost) {
        this.idV = idV;
        this.registracioniBroj = registracioniBroj;
        this.tipGoriva = tipGoriva;
        this.potrosnjalperkm = potrosnjalperkm;
        this.nosivost = nosivost;
    }

    public String getRegistracioniBroj() {
        return registracioniBroj;
    }

    public void setRegistracioniBroj(String registracioniBroj) {
        this.registracioniBroj = registracioniBroj;
    }

    public int getTipGoriva() {
        return tipGoriva;
    }

    public void setTipGoriva(int tipGoriva) {
        this.tipGoriva = tipGoriva;
    }

    public BigDecimal getPotrosnjalperkm() {
        return potrosnjalperkm;
    }

    public void setPotrosnjalperkm(BigDecimal potrosnjalperkm) {
        this.potrosnjalperkm = potrosnjalperkm;
    }

    public BigDecimal getNosivost() {
        return nosivost;
    }

    public void setNosivost(BigDecimal nosivost) {
        this.nosivost = nosivost;
    }

    public Integer getIdV() {
        return idV;
    }

    public void setIdV(Integer idV) {
        this.idV = idV;
    }

    public List<Kurir> getKurirList() {
        return kurirList;
    }

    public void setKurirList(List<Kurir> kurirList) {
        this.kurirList = kurirList;
    }

    public List<Vozi> getVoziList() {
        return voziList;
    }

    public void setVoziList(List<Vozi> voziList) {
        this.voziList = voziList;
    }

    public Adresa getLokacijaParkirano() {
        return lokacijaParkirano;
    }

    public void setLokacijaParkirano(Adresa lokacijaParkirano) {
        this.lokacijaParkirano = lokacijaParkirano;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idV != null ? idV.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vozilo)) {
            return false;
        }
        Vozilo other = (Vozilo) object;
        if ((this.idV == null && other.idV != null) || (this.idV != null && !this.idV.equals(other.idV))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Vozilo[ idV=" + idV + " ]";
    }
    
}
