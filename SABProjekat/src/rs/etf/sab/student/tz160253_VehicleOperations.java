package rs.etf.sab.student;

import rs.etf.sab.student.entities.LokacijaMagacina;
import rs.etf.sab.student.entities.Vozilo;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import rs.etf.sab.operations.VehicleOperations;

public class tz160253_VehicleOperations implements VehicleOperations {
    EntityManager em = tz160253_GeneralOperations.em;

    @Override
    public boolean insertVehicle(String licencePlateNumber, int fuelType, BigDecimal fuelConsumtion, BigDecimal capacity) {
        List<Vozilo> resultListVehicle = em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licencePlateNumber).getResultList();
        if (resultListVehicle != null && !resultListVehicle.isEmpty())
            return false;
        
        if (fuelType != 0 && fuelType != 1 && fuelType != 2)
            return false;
        
        Vozilo vehicle = new Vozilo();
        vehicle.setRegistracioniBroj(licencePlateNumber);
        vehicle.setTipGoriva(fuelType);
        vehicle.setPotrosnjalperkm(fuelConsumtion);
        vehicle.setNosivost(capacity);
        em.getTransaction().begin();
        em.persist(vehicle);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public int deleteVehicles(String... licencePlateNumbers) {
        int counter = 0;
        
        em.getTransaction().begin();
        for (String name: licencePlateNumbers){
            List<Vozilo> resultListVehicle = em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", name).getResultList();
            if (resultListVehicle == null || resultListVehicle.isEmpty())
                continue;
            em.remove(resultListVehicle.get(0));
            counter++;
        }
        em.getTransaction().commit();
        
        return counter;
    }

    @Override
    public List<String> getAllVehichles() {
        return em.createQuery("SELECT v.registracioniBroj FROM Vozilo v", String.class).getResultList();
    }

    @Override
    public boolean changeFuelType(String licensePlateNumber, int fuelType) {
        List<Vozilo> resultListVehicle = em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licensePlateNumber).getResultList();
        if (resultListVehicle == null || resultListVehicle.isEmpty())
            return false;
        Vozilo vehicle = resultListVehicle.get(0);
        
        if (fuelType != 0 && fuelType != 1 && fuelType != 2)
            return false;
        
        if (vehicle.getLokacijaParkirano() == null)
            return false;
        
        List<Vozilo> vehiclesParkedList = em.createQuery("SELECT v FROM Vozilo v WHERE v.idV = :idV AND v.lokacijaParkirano.idA IN (SELECT lm.adresa.idA FROM LokacijaMagacina lm) AND v.idV NOT IN (SELECT vz.vozilo.idV FROM Vozi vz)", Vozilo.class).setParameter("idV", vehicle.getIdV()).getResultList();
        if (vehiclesParkedList == null || vehiclesParkedList.isEmpty())
            return false;
        
        em.getTransaction().begin();
        vehicle.setTipGoriva(fuelType);
        em.getTransaction().commit();
        
        return true;
    }

    @Override
    public boolean changeConsumption(String licensePlateNumber, BigDecimal fuelConsumption) {
        List<Vozilo> resultListVehicle = em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licensePlateNumber).getResultList();
        if (resultListVehicle == null || resultListVehicle.isEmpty())
            return false;
        Vozilo vehicle = resultListVehicle.get(0);
        
        if (vehicle.getLokacijaParkirano() == null)
            return false;
        
        List<Vozilo> vehiclesParkedList = em.createQuery("SELECT v FROM Vozilo v WHERE v.idV = :idV AND v.lokacijaParkirano.idA IN (SELECT lm.adresa.idA FROM LokacijaMagacina lm) AND v.idV NOT IN (SELECT vz.vozilo.idV FROM Vozi vz)", Vozilo.class).setParameter("idV", vehicle.getIdV()).getResultList();
        if (vehiclesParkedList == null || vehiclesParkedList.isEmpty())
            return false;
        
        em.getTransaction().begin();
        vehicle.setPotrosnjalperkm(fuelConsumption);
        em.getTransaction().commit();
        
        return true;
    }

    @Override
    public boolean changeCapacity(String licensePlateNumber, BigDecimal capacity) {
        List<Vozilo> resultListVehicle = em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licensePlateNumber).getResultList();
        if (resultListVehicle == null || resultListVehicle.isEmpty())
            return false;
        Vozilo vehicle = resultListVehicle.get(0);
        
        if (vehicle.getLokacijaParkirano() == null)
            return false;
        
        List<Vozilo> vehiclesParkedList = em.createQuery("SELECT v FROM Vozilo v WHERE v.idV = :idV AND v.lokacijaParkirano.idA IN (SELECT lm.adresa.idA FROM LokacijaMagacina lm) AND v.idV NOT IN (SELECT vz.vozilo.idV FROM Vozi vz)", Vozilo.class).setParameter("idV", vehicle.getIdV()).getResultList();
        if (vehiclesParkedList == null || vehiclesParkedList.isEmpty())
            return false;
        
        em.getTransaction().begin();
        vehicle.setNosivost(capacity);
        em.getTransaction().commit();
        
        return true;
    }

    @Override
    public boolean parkVehicle(String licencePlateNumbers, int idStockroom) {
        List<Vozilo> resultListVehicle = em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licencePlateNumbers).getResultList();
        if (resultListVehicle == null || resultListVehicle.isEmpty())
            return false;
        Vozilo vehicle = resultListVehicle.get(0);
        
        LokacijaMagacina stockroom = em.find(LokacijaMagacina.class, idStockroom);
        if (stockroom == null)
            return false;
        
        List<Vozilo> vehiclesParkedList = em.createQuery("SELECT v FROM Vozilo v WHERE v.idV = :idV AND v.idV NOT IN (SELECT vz.vozilo.idV FROM Vozi vz)", Vozilo.class).setParameter("idV", vehicle.getIdV()).getResultList();
        if (vehiclesParkedList == null || vehiclesParkedList.isEmpty())
            return false;
        
        em.getTransaction().begin();
        vehicle.setLokacijaParkirano(stockroom.getAdresa());
        em.getTransaction().commit();
        
        return true;
    }
    
}
