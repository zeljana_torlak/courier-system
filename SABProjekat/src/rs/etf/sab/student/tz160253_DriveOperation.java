package rs.etf.sab.student;

import rs.etf.sab.student.entities.Adresa;
import rs.etf.sab.student.entities.Korisnik;
import rs.etf.sab.student.entities.Kurir;
import rs.etf.sab.student.entities.LokacijaMagacina;
import rs.etf.sab.student.entities.Paket;
import rs.etf.sab.student.entities.PrevoziSe;
import rs.etf.sab.student.entities.Vozilo;
import rs.etf.sab.student.entities.Vozi;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import rs.etf.sab.operations.DriveOperation;

public class tz160253_DriveOperation implements DriveOperation {
    EntityManager em = tz160253_GeneralOperations.em;

    @Override
    public boolean planingDrive(String courierUsername) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUsername).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return false;
        Korisnik user = resultListUser.get(0);
        
        Kurir courier = em.find(Kurir.class, user.getIdK());
        if (courier == null || courier.getStatus() != 0)
            return false;
        
        List<Integer> resultList = em.createQuery("SELECT vz.kurir.idK FROM Vozi vz WHERE vz.kurir.idK = :idK", Integer.class).setParameter("idK", courier.getIdK()).getResultList();
        if (resultList != null && !resultList.isEmpty())
            return false;
        
        List<LokacijaMagacina> resultListStockroom = em.createQuery("SELECT lm FROM LokacijaMagacina lm WHERE lm.adresa.grad.idG = :idG", LokacijaMagacina.class).setParameter("idG", user.getAdresa().getGrad().getIdG()).getResultList();
        if (resultListStockroom == null || resultListStockroom.isEmpty())
            return false;
        LokacijaMagacina stockroom = resultListStockroom.get(0);
        
        
        List<Vozilo> freeVehiclesInStockroom = em.createQuery("SELECT v from Vozilo v WHERE v.lokacijaParkirano.idA = :idA AND v.idV NOT IN (SELECT vz.vozilo.idV from Vozi vz)", Vozilo.class).setParameter("idA", stockroom.getAdresa().getIdA()).getResultList();
        if (freeVehiclesInStockroom == null || freeVehiclesInStockroom.isEmpty())
            return false;
        Vozilo vehicle = freeVehiclesInStockroom.get(0);
        
        List<Paket> packagesFromCity = em.createQuery("SELECT p FROM Paket p WHERE p.statusIsporuke = 1 AND p.trenutnaLokacija.grad.idG = :idG AND p.trenutnaLokacija.idA NOT IN (SELECT lm.adresa.idA FROM LokacijaMagacina lm) AND p.tezinaPaketa <= :nosivost AND p.idP NOT IN (SELECT pz.paket.idP FROM PrevoziSe pz) ORDER BY p.vremeKreiranjaZahteva", Paket.class).setParameter("idG", user.getAdresa().getGrad().getIdG()).setParameter("nosivost", vehicle.getNosivost()).getResultList();
        List<Paket> packagesFromStockroom = em.createQuery("SELECT p FROM Paket p WHERE (p.statusIsporuke = 1 OR p.statusIsporuke = 2) AND p.trenutnaLokacija.grad.idG = :idG AND p.trenutnaLokacija.idA IN (SELECT lm.adresa.idA FROM LokacijaMagacina lm) AND p.tezinaPaketa <= :nosivost AND p.idP NOT IN (SELECT pz.paket.idP FROM PrevoziSe pz) ORDER BY p.vremeKreiranjaZahteva", Paket.class).setParameter("idG", user.getAdresa().getGrad().getIdG()).setParameter("nosivost", vehicle.getNosivost()).getResultList();
            
        if ((packagesFromCity == null || packagesFromCity.isEmpty()) && (packagesFromStockroom == null || packagesFromStockroom.isEmpty()))
            return false;
             
        Vozi drive = new Vozi();
        drive.setKurir(courier);
        drive.setVozilo(vehicle);
        drive.setTrenutniProfit(BigDecimal.ZERO);
        drive.setTrenutnaLokacija(stockroom.getAdresa());
        drive.setOpterecenost(BigDecimal.ZERO);
        em.getTransaction().begin();
        em.persist(drive);
        courier.setStatus(1);
        em.getTransaction().commit();
        
        BigDecimal weightSum = BigDecimal.ZERO;
        BigDecimal priceOfPackagesSum = BigDecimal.ZERO;

        if (packagesFromCity != null){
            for (int i = 0; i < packagesFromCity.size(); i++){
                Paket pack = packagesFromCity.get(i);
                
                if (weightSum.add(pack.getTezinaPaketa()).subtract(vehicle.getNosivost()).signum() == 1)
                    continue;
                
                PrevoziSe packTransport = new PrevoziSe();
                packTransport.setVozi(drive);
                packTransport.setPaket(pack);
                packTransport.setRedniBroj(1);
                em.getTransaction().begin();
                em.persist(packTransport);
                em.getTransaction().commit();
                weightSum = weightSum.add(pack.getTezinaPaketa());
                priceOfPackagesSum = priceOfPackagesSum.add(pack.getCena());
            }
        }
        
        if (packagesFromStockroom != null){
            for (int i = 0; i < packagesFromStockroom.size(); i++){
                Paket pack = packagesFromStockroom.get(i);
                
                if (weightSum.add(pack.getTezinaPaketa()).subtract(vehicle.getNosivost()).signum() == 1)
                    continue;
                
                PrevoziSe packTransport = new PrevoziSe();
                packTransport.setVozi(drive);
                packTransport.setPaket(pack);
                packTransport.setRedniBroj(1);
                em.getTransaction().begin();
                em.persist(packTransport);
                em.getTransaction().commit();
                weightSum = weightSum.add(pack.getTezinaPaketa());
                priceOfPackagesSum = priceOfPackagesSum.add(pack.getCena());
            }
        }
        
        em.getTransaction().begin();
        drive.setTrenutniProfit(priceOfPackagesSum);
        drive.setOpterecenost(weightSum);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public int nextStop(String courierUsername) {
        List<Korisnik> resultListUser = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUsername).getResultList();
        if (resultListUser == null || resultListUser.isEmpty())
            return -4;
        Korisnik user = resultListUser.get(0);
        
        Kurir courier = em.find(Kurir.class, user.getIdK());
        if (courier == null || courier.getStatus() != 1)
            return -4;
        
        List<Vozi> resultListDrive = em.createQuery("SELECT vz FROM Vozi vz WHERE vz.kurir.idK = :idK", Vozi.class).setParameter("idK", user.getIdK()).getResultList();
        if (resultListDrive == null || resultListDrive.isEmpty())
            return -4;
        Vozi drive = resultListDrive.get(0);
        
        Vozilo vehicle = em.find(Vozilo.class, drive.getVozilo().getIdV());
        if (vehicle == null)
            return -4;
        
        BigDecimal price = vehicle.getPotrosnjalperkm();
        switch (vehicle.getTipGoriva()){ //*cena_per_l
            case 0:
                price = price.multiply(new BigDecimal(15));
                break;
            case 1:
                price = price.multiply(new BigDecimal(32));
                break;
            case 2:
                price = price.multiply(new BigDecimal(36));
                break;
        }
        
        List<LokacijaMagacina> resultListStockroom = em.createQuery("SELECT lm FROM LokacijaMagacina lm WHERE lm.adresa.grad.idG = :idG", LokacijaMagacina.class).setParameter("idG", user.getAdresa().getGrad().getIdG()).getResultList();
        if (resultListStockroom == null || resultListStockroom.isEmpty())
            return -4;
        LokacijaMagacina stockroom = resultListStockroom.get(0);
        Adresa stockroomLocation = stockroom.getAdresa();
        
        List<PrevoziSe> packsTransport1 = em.createQuery("SELECT pz FROM PrevoziSe pz WHERE pz.vozi.idVozi = :idVozi AND pz.redniBroj = 1 ORDER BY pz.redniBroj, pz.idPrevoziSe", PrevoziSe.class).setParameter("idVozi", drive.getIdVozi()).getResultList();
        List<PrevoziSe> packsTransport2 = em.createQuery("SELECT pz FROM PrevoziSe pz WHERE pz.vozi.idVozi = :idVozi AND pz.redniBroj > 1 ORDER BY pz.idPrevoziSe", PrevoziSe.class).setParameter("idVozi", drive.getIdVozi()).getResultList();
        List<PrevoziSe> packsTransport3 = em.createQuery("SELECT pz FROM PrevoziSe pz WHERE pz.vozi.idVozi = :idVozi AND pz.redniBroj = 0 ORDER BY pz.idPrevoziSe", PrevoziSe.class).setParameter("idVozi", drive.getIdVozi()).getResultList();
        List<PrevoziSe> packsTransport4 = em.createQuery("SELECT pz FROM PrevoziSe pz WHERE pz.vozi.idVozi = :idVozi AND pz.redniBroj < 0 ORDER BY pz.redniBroj DESC, pz.idPrevoziSe", PrevoziSe.class).setParameter("idVozi", drive.getIdVozi()).getResultList();
        
        if (packsTransport1 != null && !packsTransport1.isEmpty()){
            takePackagesFromStartingCity(drive, packsTransport1, price);
            return -2;
        }
        
        if (packsTransport2 != null && !packsTransport2.isEmpty()){
            takePackagesFromVisitingCity(drive, packsTransport2, price);            
            return -2;
        }

        if (packsTransport3 != null && !packsTransport3.isEmpty()){
            return deliverPackages(drive, packsTransport3, price, courier, vehicle);
        }
        
        if (packsTransport4 != null && !packsTransport4.isEmpty()){
            returnPackagesToStockroom(stockroomLocation, packsTransport4);
        }
        
        //kraj voznje
        BigDecimal distance = new BigDecimal(calcDist(drive.getTrenutnaLokacija(), stockroomLocation));
        BigDecimal profit = drive.getTrenutniProfit().subtract(price.multiply(distance));
        em.getTransaction().begin();
        courier.setStatus(0);
        courier.setOstvarenProfit(courier.getOstvarenProfit().add(profit));
        em.remove(drive);
        em.getTransaction().commit();
        
        return -1;
    }

    @Override
    public List<Integer> getPackagesInVehicle(String courierUsername) {
        return em.createQuery("SELECT pz.paket.idP FROM PrevoziSe pz, Vozi vz WHERE pz.vozi.idVozi = vz.idVozi AND pz.redniBroj <= 0 AND vz.kurir.idK IN (SELECT k.idK FROM Korisnik k WHERE k.korisnickoIme = :username)", Integer.class).setParameter("username", courierUsername).getResultList();
    }
    
    private void takePackagesFromStartingCity(Vozi drive, List<PrevoziSe> packsTransport1, BigDecimal price){
        PrevoziSe packTransp = packsTransport1.remove(0);
        Paket pack = packTransp.getPaket();
        BigDecimal dist = new BigDecimal(calcDist(drive.getTrenutnaLokacija(), pack.getTrenutnaLokacija()));
        price = price.multiply(dist);
                
        em.getTransaction().begin();
        pack.setStatusIsporuke(2);
        packTransp.setRedniBroj(0);
        drive.setTrenutniProfit(drive.getTrenutniProfit().subtract(price));
        drive.setTrenutnaLokacija(pack.getTrenutnaLokacija());
        em.getTransaction().commit();
            
        //preuzimanje vise paketa sa iste adrese
        for (int i = 0; i < packsTransport1.size(); i++){
            PrevoziSe packTranspTmp = packsTransport1.get(i);
            if (packTranspTmp.getPaket().getTrenutnaLokacija().getIdA() != pack.getTrenutnaLokacija().getIdA())
                continue;
                
            em.getTransaction().begin();
            packTranspTmp.getPaket().setStatusIsporuke(2);
            packTranspTmp.setRedniBroj(0);
            em.getTransaction().commit();
        }
    }
    
    private void takePackagesFromVisitingCity(Vozi drive, List<PrevoziSe> packsTransport2, BigDecimal price){
        PrevoziSe packTransp = packsTransport2.remove(0);
        Paket pack = packTransp.getPaket();
        BigDecimal dist = new BigDecimal(calcDist(drive.getTrenutnaLokacija(), pack.getTrenutnaLokacija()));
        price = price.multiply(dist);
               
        em.getTransaction().begin();
        pack.setStatusIsporuke(2);
        packTransp.setRedniBroj(-1);
        drive.setTrenutniProfit(drive.getTrenutniProfit().subtract(price));
        drive.setTrenutnaLokacija(pack.getTrenutnaLokacija());
        em.getTransaction().commit();
            
        //preuzimanje vise paketa sa iste adrese
        for (int i = 0; i < packsTransport2.size(); i++){
            PrevoziSe packTranspTmp = packsTransport2.get(i);
            if (packTranspTmp.getPaket().getTrenutnaLokacija().getIdA() != pack.getTrenutnaLokacija().getIdA())
                continue;
              
            em.getTransaction().begin();
            packTranspTmp.getPaket().setStatusIsporuke(2);
            packTranspTmp.setRedniBroj(-1);
            em.getTransaction().commit();
        }
    }
    
    public int deliverPackages(Vozi drive, List<PrevoziSe> packsTransport3, BigDecimal price, Kurir courier, Vozilo vehicle){
        PrevoziSe packTransp = packsTransport3.get(0);
        Paket pack = packTransp.getPaket();
        double dist = calcDist(drive.getTrenutnaLokacija(), pack.getAdresaZaDostavljanje());

        for (int i = 1; i < packsTransport3.size(); i++){
            PrevoziSe packTranspTmp = packsTransport3.get(i);
            Paket packTmp = packTranspTmp.getPaket();
            double dist2 = calcDist(drive.getTrenutnaLokacija(), packTmp.getAdresaZaDostavljanje());
            if (dist2 < dist){
                packTransp = packTranspTmp;
                pack = packTmp;
                dist = dist2;
            }
        }
        
        price = price.multiply(new BigDecimal(dist));
        em.getTransaction().begin();
        pack.setStatusIsporuke(3);
        pack.setTrenutnaLokacija(pack.getAdresaZaDostavljanje());
        em.remove(packTransp);
        drive.setTrenutniProfit(drive.getTrenutniProfit().subtract(price));
        drive.setOpterecenost(drive.getOpterecenost().subtract(pack.getTezinaPaketa()));
        drive.setTrenutnaLokacija(pack.getAdresaZaDostavljanje());
        courier.setBrojIsporucenihPaketa(courier.getBrojIsporucenihPaketa() + 1);
        em.getTransaction().commit();
            
        packsTransport3.remove(packTransp);
        boolean morePacksInSameCity = false;
        //isporuka vise paketa na istu adresu
        for (int i = 0; i < packsTransport3.size(); i++){
            PrevoziSe packTranspTmp = packsTransport3.get(i);
            if (packTranspTmp.getPaket().getAdresaZaDostavljanje().getIdA() != pack.getAdresaZaDostavljanje().getIdA()){
                if (packTranspTmp.getPaket().getAdresaZaDostavljanje().getGrad().getIdG() == pack.getAdresaZaDostavljanje().getGrad().getIdG())
                    morePacksInSameCity = true;
                continue;
            }
            em.getTransaction().begin();
            packTranspTmp.getPaket().setStatusIsporuke(3);
            packTranspTmp.getPaket().setTrenutnaLokacija(packTranspTmp.getPaket().getAdresaZaDostavljanje());
            em.remove(packTranspTmp);
            drive.setOpterecenost(drive.getOpterecenost().subtract(packTranspTmp.getPaket().getTezinaPaketa()));
            courier.setBrojIsporucenihPaketa(courier.getBrojIsporucenihPaketa() + 1);
            em.getTransaction().commit();
        }
            
        if (!morePacksInSameCity){
            List<Paket> packagesFromCity = em.createQuery("SELECT p FROM Paket p WHERE p.statusIsporuke = 1 AND p.trenutnaLokacija.grad.idG = :idG AND p.trenutnaLokacija.idA NOT IN (SELECT lm.adresa.idA FROM LokacijaMagacina lm) AND p.tezinaPaketa <= :nosivost AND p.idP NOT IN (SELECT pz.paket.idP FROM PrevoziSe pz) ORDER BY p.vremeKreiranjaZahteva", Paket.class).setParameter("idG", drive.getTrenutnaLokacija().getGrad().getIdG()).setParameter("nosivost", vehicle.getNosivost().subtract(drive.getOpterecenost())).getResultList();
            List<Paket> packagesFromStockroom = em.createQuery("SELECT p FROM Paket p WHERE (p.statusIsporuke = 1 OR p.statusIsporuke = 2) AND p.trenutnaLokacija.grad.idG = :idG AND p.trenutnaLokacija.idA IN (SELECT lm.adresa.idA FROM LokacijaMagacina lm) AND p.tezinaPaketa <= :nosivost AND p.idP NOT IN (SELECT pz.paket.idP FROM PrevoziSe pz) ORDER BY p.vremeKreiranjaZahteva", Paket.class).setParameter("idG", drive.getTrenutnaLokacija().getGrad().getIdG()).setParameter("nosivost", vehicle.getNosivost().subtract(drive.getOpterecenost())).getResultList();

            if (packagesFromCity != null){
                for (int i = 0; i < packagesFromCity.size(); i++){
                    Paket pack1 = packagesFromCity.get(i);

                    if (drive.getOpterecenost().add(pack1.getTezinaPaketa()).subtract(vehicle.getNosivost()).signum() == 1)
                        continue;

                    PrevoziSe packTransport = new PrevoziSe();
                    packTransport.setVozi(drive);
                    packTransport.setPaket(pack1);
                    packTransport.setRedniBroj(2);
                    em.getTransaction().begin();
                    em.persist(packTransport);
                    drive.setOpterecenost(drive.getOpterecenost().add(pack1.getTezinaPaketa()));
                    em.getTransaction().commit();
                }
            }

            if (packagesFromStockroom != null){
                for (int i = 0; i < packagesFromStockroom.size(); i++){
                    Paket pack2 = packagesFromStockroom.get(i);

                    if (drive.getOpterecenost().add(pack2.getTezinaPaketa()).subtract(vehicle.getNosivost()).signum() == 1)
                        continue;

                    PrevoziSe packTransport = new PrevoziSe();
                    packTransport.setVozi(drive);
                    packTransport.setPaket(pack2);
                    packTransport.setRedniBroj(2);
                    em.getTransaction().begin();
                    em.persist(packTransport);
                    drive.setOpterecenost(drive.getOpterecenost().add(pack2.getTezinaPaketa()));
                    em.getTransaction().commit();
                }
            }
        }
            
        return pack.getIdP();
    }
    
    private void returnPackagesToStockroom(Adresa stockroomLocation, List<PrevoziSe> packsTransport4){
        for (int i = 0; i < packsTransport4.size(); i++){
            PrevoziSe packTranspTmp = packsTransport4.get(i);
            em.getTransaction().begin();
            packTranspTmp.getPaket().setTrenutnaLokacija(stockroomLocation);
            em.remove(packTranspTmp);
            em.getTransaction().commit();
        }
    }
    
    private double calcDist(Adresa adr1, Adresa adr2){
        return Math.sqrt(Math.pow(adr1.getXkm()-adr2.getXkm(), 2) + Math.pow(adr1.getYkm()-adr2.getYkm(), 2));
    }
    
}
